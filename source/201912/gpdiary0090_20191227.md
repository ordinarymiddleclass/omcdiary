# 蒼白球日誌0090_gpdiary0090_20191227 #

## 日期 Date ##

* 世界協調時間2019年(中華民國108年，令和1年)12月27日 / Unix 紀元 18257 日 / 星期五 / 蒼白球紀元第90日
* December 27, 2019 (UTC) / 18257 days since Unix Epoch / Friday / Globus Pallidum day 90
* 特殊註記:

## 年齡 Age ##

* 33 years 8 months 4 days old / 2 years 2 months 15 days after acquiring ROC Surgical Pathology Licence
* 33 歲 8 個月 4 天 / 成為病理專科醫師 2 年 2 個月 15 天

## 本文 Content ##

1. 

    
2. 雜記:物價與其他[2]

    

## 注釋 Comment ##

[1] 


[2] 新台幣計價。有關新台幣請參見蒼白球日誌0007。



## 附錄 Appendix ##

