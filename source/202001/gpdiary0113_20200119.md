# 蒼白球日誌0113_gpdiary0113_20200119 #

## 日期 Date ##

* 世界協調時間2020年(中華民國109年，令和2年)1月19日 / Unix 紀元 18280 日 / 星期日 / 蒼白球紀元第113日
* January 19, 2020 (UTC) / 18280 days since Unix Epoch / Sunday / Globus Pallidum day 113
* 特殊註記:

## 年齡 Age ##

* 33 years 8 months 27 days old / 2 years 3 months 7 days after acquiring ROC Surgical Pathology Licence
* 33 歲 8 個月 27 天 / 成為病理專科醫師 2 年 3 個月 7 天

## 本文 Content ##

1. 

    
2. 雜記:物價與其他[2]

    

## 注釋 Comment ##

[1] 


[2] 新台幣計價。有關新台幣請參見蒼白球日誌0007。



## 附錄 Appendix ##

