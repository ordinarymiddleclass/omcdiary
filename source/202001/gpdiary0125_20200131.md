# 蒼白球日誌0125_gpdiary0125_20200131 #

## 日期 Date ##

* 世界協調時間2020年(中華民國109年，令和2年)1月31日 / Unix 紀元 18292 日 / 星期五 / 蒼白球紀元第125日
* January 31, 2020 (UTC) / 18292 days since Unix Epoch / Friday / Globus Pallidum day 125
* 特殊註記:

## 年齡 Age ##

* 33 years 9 months 8 days old / 2 years 3 months 19 days after acquiring ROC Surgical Pathology Licence
* 33 歲 9 個月 8 天 / 成為病理專科醫師 2 年 3 個月 19 天

## 本文 Content ##

1. 

    
2. 雜記:物價與其他[2]

    

## 注釋 Comment ##

[1] 


[2] 新台幣計價。有關新台幣請參見蒼白球日誌0007。



## 附錄 Appendix ##

