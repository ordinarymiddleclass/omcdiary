# 蒼白球日誌0118_gpdiary0118_20200124 #

## 日期 Date ##

* 世界協調時間2020年(中華民國109年，令和2年)1月24日 / Unix 紀元 18285 日 / 星期五 / 蒼白球紀元第118日
* January 24, 2020 (UTC) / 18285 days since Unix Epoch / Friday / Globus Pallidum day 118
* 特殊註記:

## 年齡 Age ##

* 33 years 9 months 1 days old / 2 years 3 months 12 days after acquiring ROC Surgical Pathology Licence
* 33 歲 9 個月 1 天 / 成為病理專科醫師 2 年 3 個月 12 天

## 本文 Content ##

1. 

    
2. 雜記:物價與其他[2]

    

## 注釋 Comment ##

[1] 


[2] 新台幣計價。有關新台幣請參見蒼白球日誌0007。



## 附錄 Appendix ##

