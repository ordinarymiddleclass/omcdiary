# 蒼白球日誌0061_gpdiary0061_20191128 #

## 日期 Date ##

* 世界協調時間2019年(中華民國108年，令和1年)11月28日 / Unix 紀元 18228 日 / 星期四 / 蒼白球紀元第61日
* November 28, 2019 (UTC) / 18228 days since Unix Epoch / Thursday / Globus Pallidum day 61
* 特殊註記:

## 年齡 Age ##

* 33 years 7 months 5 days old / 2 years 1 months 16 days after acquiring ROC Surgical Pathology Licence
* 33 歲 7 個月 5 天 / 成為病理專科醫師 2 年 1 個月 16 天

## 本文 Content ##

1. 

    
2. 雜記:物價與其他[2]

    

## 注釋 Comment ##

[1] 


[2] 新台幣計價。有關新台幣請參見蒼白球日誌0007。



## 附錄 Appendix ##

